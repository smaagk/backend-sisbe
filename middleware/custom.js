const { Aspirante } = require('../models');
const { to, responseError } = require('../services/util.services');
const _ = require('lodash');
const jwt = require('jsonwebtoken');

module.exports = {
    async aspirante(req, res, next) {
      const { email } = req.params;
      console.log(req.body);
      console.log(email);
      const [error, aspirante] = await to(Aspirante.findOne({ where: { email: email } }));
  
      if (error) return responseError(res, 'Error al encontrar al aspirante');
  
      if (!aspirante) return responseError(res, "Cuenta inexistente");
  
      req.aspirante = aspirante;
      req.email = req.body.email;
      req.password = req.body.password;
      return next();
    },
    checkToken(req, res, next){
        let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase      
        if (token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length);
        }
        
        if (token) {
          console.log(token)
          jwt.verify(token, 'lukeyosoytupadre', (err, decoded) => {
            if (err) {
              return res.json({
                success: false,
                message: 'El token no es valido'
              });
            } else {
              req.decoded = decoded;
              req.ingresos = req.body.ingresos
              req.egresos = req.body.egresos
              req.email = req.body.email;
              return next();
            }
          });
        } else {
          return res.json({
            success: false,
            message: 'Auth token no fue indicado'
          });
        }
      }
  };