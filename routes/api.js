const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const customMiddleware = require("../middleware/custom");
const models = require("../models");

const AspiranteController = require("../controllers/aspirante.controller");
const DatosPersonalesController = require("../controllers/datos-personales.controller");
const IngresosEgresosController = require("../controllers/ingresos_egresos");
const DatosAcademicosController = require("../controllers/datos-academicos.controller");
const DatosContactos = require("../controllers/datos-contactos.controller");
const DatosVivienda = require("../controllers/datos_vivienda.controller");
const DatosTransporte = require("../controllers/datos-medios-transporte.controller");
const DatosIngresosFamiliares = require("../controllers/datos-ingresos-familiares.controller");

const DatosCulturales = require("../controllers/datos-culturales.controller");
const StatusForms = require("../controllers/status-forms.controller");
const DatosArchivos = require("../controllers/archivos-upload.controller");
const Solicitudes = require("../controllers/solicitud.controller");

const pdf = require('../controllers/pdf.controller')

router.get("/api", (req, res, next) => {
  res.json({
    status: "success",
    message: "Parcel Pending API",
    data: { version_number: "v1.0.0" }
  });
  return next();
});

router.get("/confirmation/:emailToken", async (req, res) => {
  try {
    const {
      aspirante: { id }
    } = jwt.verify(req.params.emailToken, "lukeyosoytupadre");
    await models.Aspirante.update(
      { estado_cuenta: "activa" },
      { where: { id } }
    );
  } catch (e) {
    return res.send("error");
  }

  return res.redirect(
    `http://165.22.178.175/password/${req.params.emailToken}`
  );
  //return res.redirect(`http://localhost:4200/password/${req.params.emailToken}`);
});

router.get("/recoverpassword/:emailToken", async (req, res) => {
  return res.redirect(
    `http://165.22.178.175/password/${req.params.emailToken}`
  );
  //return res.redirect(`http://localhost:4200/password/${req.params.emailToken}`)
});

router.get("/aspirantes", AspiranteController.getAll);
router.post("/aspirantes", AspiranteController.create);
router.get(
  "/aspirantes/:email",
  customMiddleware.aspirante,
  AspiranteController.resendTokenVerification
);
router.get(
  "/aspirantes/recover/:email",
  customMiddleware.aspirante,
  AspiranteController.recoverPassword
);
router.put(
  "/aspirantes/password/",
  customMiddleware.checkToken,
  AspiranteController.updatePassword
);
//router.put('/aspirantes',customMiddleware.checkToken,AspiranteController.updateData);

router.post(
  "/datospersonales",
  customMiddleware.checkToken,
  DatosPersonalesController.createOrUpdate
);
router.get(
  "/datospersonales",
  customMiddleware.checkToken,
  DatosPersonalesController.getDatospersonales
);

router.post(
  "/datosacademicos",
  customMiddleware.checkToken,
  DatosAcademicosController.createOrUpdate
);
router.get(
  "/datosacademicos",
  customMiddleware.checkToken,
  DatosAcademicosController.getDatosacademicos
);

router.post(
  "/ingresosegresos",
  customMiddleware.checkToken,
  IngresosEgresosController.create_update
);
router.get(
  "/ingresos",
  customMiddleware.checkToken,
  IngresosEgresosController.getIngresos
);
router.get(
  "/egresos",
  customMiddleware.checkToken,
  IngresosEgresosController.getEgresos
);

router.post(
  "/datoscontactos",
  customMiddleware.checkToken,
  DatosContactos.createOrUpdate
);
router.get(
  "/datoscontactos",
  customMiddleware.checkToken,
  DatosContactos.getDatosContactos
);

router.get(
  "/datosvivienda",
  customMiddleware.checkToken,
  DatosVivienda.getDatosvivienda
);
router.post(
  "/datosvivienda",
  customMiddleware.checkToken,
  DatosVivienda.createOrUpdate
);

router.get(
  "/datostransporte",
  customMiddleware.checkToken,
  DatosTransporte.getDatosMediosDeTransporte
);
router.post(
  "/datostransporte",
  customMiddleware.checkToken,
  DatosTransporte.createOrUpdate
);

router.get(
  "/datosingresosfamiliares",
  customMiddleware.checkToken,
  DatosIngresosFamiliares.getDatosIngresoFamiliar
);
router.post(
  "/datosingresosfamiliares",
  customMiddleware.checkToken,
  DatosIngresosFamiliares.createOrUpdate
);

router.get(
  "/datosculturales",
  customMiddleware.checkToken,
  DatosCulturales.getDatosCulturales
);
router.post(
  "/datosculturales",
  customMiddleware.checkToken,
  DatosCulturales.createOrUpdate
);

router.get(
  "/status-form",
  customMiddleware.checkToken,
  StatusForms.getStatusFormularios
);
router.post(
  "/status-form",
  customMiddleware.checkToken,
  StatusForms.createOrUpdate
);

router.get("/upload", customMiddleware.checkToken, DatosArchivos.getFiles);
router.post("/upload", customMiddleware.checkToken, DatosArchivos.create);

router.post(
  "/deleteFile",
  customMiddleware.checkToken,
  DatosArchivos.deleteFile
);

router.get("/solicitud", customMiddleware.checkToken, Solicitudes.getSolicitud);
router.post("/solicitud", customMiddleware.checkToken, Solicitudes.createOrUpdate);

router.post('/generate-pdf',pdf.generate)

router.post("/login", AspiranteController.auth);
//router.delete('/aspirantes',AspiranteController.delete);

module.exports = router;
