import os
#import magic
import urllib.request
from app import app
from flask import Flask, flash, request, redirect, render_template
from flask import jsonify

from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
	
@app.route('/')
def upload_form():
	return render_template('upload.html')

@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save('./files/'+secure_filename(f.filename))
        return jsonify({'res' : 'file uploaded successfully'})

if __name__ == "__main__":
    app.run()