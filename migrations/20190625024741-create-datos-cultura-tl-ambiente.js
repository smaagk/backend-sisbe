'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DatosCulturaTlAmbientes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      discapacidad: {
        type: Sequelize.STRING
      },
      discapacidad_familiar: {
        type: Sequelize.BOOLEAN
      },
      deporte: {
        type: Sequelize.STRING
      },
      frecuencia_deporte: {
        type: Sequelize.STRING
      },
      actividad_artistica: {
        type: Sequelize.STRING
      },
      tiempo_libre: {
        type: Sequelize.STRING
      },
      p_contaminacion: {
        type: Sequelize.STRING
      },
      p_escasez_agua: {
        type: Sequelize.STRING
      },
      p_energia: {
        type: Sequelize.STRING
      },
      p_areasverdes: {
        type: Sequelize.STRING
      },
      p_contaminacion_agua: {
        type: Sequelize.STRING
      },
      reciclaje: {
        type: Sequelize.STRING
      },
      programa_proteccion_ambiental: {
        type: Sequelize.BOOLEAN
      },
      programa_servicio: {
        type: Sequelize.BOOLEAN
      },
      tipo_programa: {
        type: Sequelize.STRING
      },
      email_aspirante: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DatosCulturaTlAmbientes');
  }
};