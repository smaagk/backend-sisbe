'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DatosMediosDeTransportes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      taxi: {
        type: Sequelize.STRING
      },
      automovil_propio: {
        type: Sequelize.STRING
      },
      metro: {
        type: Sequelize.STRING
      },
      metrobus: {
        type: Sequelize.STRING
      },
      combi: {
        type: Sequelize.STRING
      },
      suburbano: {
        type: Sequelize.STRING
      },
      otro: {
        type: Sequelize.STRING
      },
      email_aspirante: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DatosMediosDeTransportes');
  }
};