'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DatosPersonales', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        type: Sequelize.STRING
      },
      primer_apellido: {
        type: Sequelize.STRING
      },
      segundo_apellido: {
        type: Sequelize.STRING
      },
      curp: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      numero_celular: {
        type: Sequelize.STRING
      },
      numero_fijo: {
        type: Sequelize.STRING
      },
      domicilio: {
        type: Sequelize.STRING
      },
      alcaldia_municipio: {
        type: Sequelize.STRING
      },
      entidad_federativa: {
        type: Sequelize.STRING
      },
      cp: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DatosPersonales');
  }
};