'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DatosIngresoFamiliars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      no_integrantes: {
        type: Sequelize.STRING
      },
      trabaja: {
        type: Sequelize.BOOLEAN
      },
      viviendo_con: {
        type: Sequelize.STRING
      },
      dependencia: {
        type: Sequelize.BOOLEAN
      },
      sosten: {
        type: Sequelize.STRING
      },
      no_personas_sosten_padres: {
        type: Sequelize.STRING
      },
      no_personas_aporte: {
        type: Sequelize.STRING
      },
      servicio_medico: {
        type: Sequelize.STRING
      },
      clase_social: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DatosIngresoFamiliars');
  }
};