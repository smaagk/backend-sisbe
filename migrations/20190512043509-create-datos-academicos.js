'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DatosAcademicos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      boleta: {
        type: Sequelize.STRING
      },
      no_creditos: {
        type: Sequelize.STRING
      },
      promedio: {
        type: Sequelize.STRING
      },
      situacion: {
        type: Sequelize.STRING
      },
      unidad: {
        type: Sequelize.STRING
      },
      modalidad: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DatosAcademicos');
  }
};