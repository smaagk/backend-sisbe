'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DatosViviendas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      residencia_actual: {
        type: Sequelize.STRING
      },
      propiedad_estatus: {
        type: Sequelize.STRING
      },
      zona: {
        type: Sequelize.STRING
      },
      computadora: {
        type: Sequelize.BOOLEAN
      },
      telefono: {
        type: Sequelize.BOOLEAN
      },
      lavadora: {
        type: Sequelize.BOOLEAN
      },
      licuadora: {
        type: Sequelize.BOOLEAN
      },
      television: {
        type: Sequelize.BOOLEAN
      },
      refigerador: {
        type: Sequelize.BOOLEAN
      },
      celular: {
        type: Sequelize.BOOLEAN
      },
      internet: {
        type: Sequelize.BOOLEAN
      },
      agua_potable: {
        type: Sequelize.BOOLEAN
      },
      luz: {
        type: Sequelize.BOOLEAN
      },
      gas: {
        type: Sequelize.BOOLEAN
      },
      drenaje: {
        type: Sequelize.BOOLEAN
      },
      cuarto_cocina: {
        type: Sequelize.BOOLEAN
      },
      no_baños: {
        type: Sequelize.STRING
      },
      no_recamaras: {
        type: Sequelize.STRING
      },
      personas_viviendo: {
        type: Sequelize.STRING
      },
      email_aspirante: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DatosViviendas');
  }
};