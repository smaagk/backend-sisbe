'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('statusFormularios', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      datos_personales: {
        type: Sequelize.BOOLEAN
      },
      estudio_socioeconomico: {
        type: Sequelize.BOOLEAN
      },
      ingresos_egresos: {
        type: Sequelize.BOOLEAN
      },
      archivos: {
        type: Sequelize.BOOLEAN
      },
      email_aspirante: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('statusFormularios');
  }
};