'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Egresos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      renta: {
        type: Sequelize.STRING
      },
      agua: {
        type: Sequelize.STRING
      },
      luz: {
        type: Sequelize.STRING
      },
      predial: {
        type: Sequelize.STRING
      },
      telefono: {
        type: Sequelize.STRING
      },
      gas: {
        type: Sequelize.STRING
      },
      alimentacion: {
        type: Sequelize.STRING
      },
      transportacion: {
        type: Sequelize.STRING
      },
      celulares: {
        type: Sequelize.STRING
      },
      otros: {
        type: Sequelize.STRING
      },
      email_aspirante: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Egresos');
  }
};