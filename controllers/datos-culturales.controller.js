const { DatosCulturales } = require("../models");
const {
  to,
  responseError,
  responseSuccess
} = require("../services/util.services");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const customMiddleware = require("../middleware/custom");

module.exports = {
  async createOrUpdate(req, res) {
    const {
      discapacidad,
      discapacidad_familiar,
      deporte,
      frecuencia_deporte,
      actividad_artistica,
      tiempo_libre,
      p_contaminacion,
      p_escasez_agua,
      p_energia,
      p_areasverdes,
      p_contaminacion_agua,
      reciclaje,
      programa_proteccion_ambiental,
      programa_servicio,
      tipo_programa
    } = req.body;
    const email = req.decoded.aspirante.email;
 console.log(discapacidad)
    const [error, datosCulturales] = await to(
      DatosCulturales.findOrCreate({
        where: { email_aspirante: email },
        defaults: {
          discapacidad,
          discapacidad_familiar,
          deporte,
          frecuencia_deporte,
          actividad_artistica,
          tiempo_libre,
          p_contaminacion,
          p_escasez_agua,
          p_energia,
          p_areasverdes,
          p_contaminacion_agua,
          reciclaje,
          programa_proteccion_ambiental,
          programa_servicio,
          tipo_programa
        }
      })
    );

    if (error) {
      console.log(error);
      return responseError(res, error, 422);
    }

    if (datosCulturales[1] == true) {
      return responseSuccess(
        res,
        { datos: datosCulturales[0].dataValues, created: datosCulturales[1] },
        201
      );
    } else if (datosCulturales[1] == false) {
      try {
        await DatosCulturales.update(
            {
                discapacidad,
                discapacidad_familiar,
                deporte,
                frecuencia_deporte,
                actividad_artistica,
                tiempo_libre,
                p_contaminacion,
                p_escasez_agua,
                p_energia,
                p_areasverdes,
                p_contaminacion_agua,
                reciclaje,
                programa_proteccion_ambiental,
                programa_servicio,
                tipo_programa
              },
          { where: { email_aspirante: email } }
        );
      } catch (e) {
        return res.send("error");
      }
      return responseSuccess(res, { message: "datos actualizados" });
    }
  },
  async getDatosCulturales(req, res) {
    const email_aspirante = req.decoded.aspirante.email;
    const [error, datosCulturales] = await to(
      DatosCulturales.findAll({ where: { email_aspirante } })
    );

    if (error) return responseError(res, error, 422);

    return responseSuccess(res, { datosCulturales });
  }
};
