const { solicitudes } = require("../models");
const {
  to,
  responseError,
  responseSuccess
} = require("../services/util.services");

module.exports = {
  async createOrUpdate(req, res) {
    const data = req.body;
    const email = req.decoded.aspirante.email;
    console.log(data);
    const [error, _solicitudes] = await to(
      solicitudes.findOrCreate({
        where: { email_aspirante: email },
        defaults: {
          boleta: data.boleta,
          nombre: data.nombre,
          primer_apellido: data.primer_apellido,
          segundo_apellido: data.segundo_apellido,
          estatus: data.estatus,
          ciclo_escolar: data.ciclo_escolar
        }
      })
    );

    if (error) return responseError(res, error, 422);

    if (_solicitudes[1] == true) {
      return responseSuccess(
        res,
        { datos: _solicitudes[0].dataValues, created: _solicitudes[1] },
        201
      );
    }
  },
  async getSolicitud(req, res) {
    const email = req.decoded.aspirante.email;
    const [error, _solicitudes] = await to(
        solicitudes.findAll({ where: { email_aspirante: email } })
    );

    if (error) return responseError(res, error, 422);

    return responseSuccess(res, { _solicitudes });
  }
};
