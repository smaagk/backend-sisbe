const { DatosVivienda } = require("../models");
const {
  to,
  responseError,
  responseSuccess
} = require("../services/util.services");

module.exports = {
  async createOrUpdate(req, res) {
    const {
      residencia_actual,
      propiedad_estatus,
      zona,
      computadora,
      telefono,
      lavadora,
      licuadora,
      television,
      refrigerador,
      celular,
      internet,
      agua_potable,
      luz,
      gas,
      drenaje,
      cuarto_cocina,
      no_banos,
      no_recamaras,
      personas_viviendo
    } = req.body;
    const email = req.decoded.aspirante.email;
    console.log(residencia_actual);
    const [error, datosVivienda] = await to(
      DatosVivienda.findOrCreate({
        where: { email_aspirante: email },
        defaults: {
          residencia_actual,
          propiedad_estatus,
          zona,
          computadora,
          telefono,
          lavadora,
          licuadora,
          television,
          refrigerador,
          celular,
          internet,
          agua_potable,
          luz,
          gas,
          drenaje,
          cuarto_cocina,
          no_banos,
          no_recamaras,
          personas_viviendo
        }
      })
    );

    if (error) return responseError(res, error, 422);

    if (datosVivienda[1] == true) {
      return responseSuccess(
        res,
        { datos: datosVivienda[0].dataValues, created: DatosVivienda[1] },
        201
      );
    } else if (datosVivienda[1] == false) {
      try {
        await DatosVivienda.update(
          {
            residencia_actual,
            propiedad_estatus,
            zona,
            computadora,
            telefono,
            lavadora,
            licuadora,
            television,
            refrigerador,
            celular,
            internet,
            agua_potable,
            luz,
            gas,
            drenaje,
            cuarto_cocina,
            no_banos,
            no_recamaras,
            personas_viviendo 
      
          },
          { where: { email_aspirante: email } }
        );
      } catch (e) {
        return res.send("error");
      }

      return responseSuccess(res, { message: "datos actualizados" });
    }
  },
  async getDatosvivienda(req, res) {
    const email = req.decoded.aspirante.email;
    const [error, datosvivienda] = await to(
      DatosVivienda.findAll({ where: { email_aspirante: email } })
    );

    if (error) return responseError(res, error, 422);

    return responseSuccess(res, { datosvivienda });
  }
};
