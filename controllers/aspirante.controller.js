const { Aspirante } = require('../models');
const { to, responseError, responseSuccess } = require('../services/util.services');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken')
const _ = require('lodash');
const customMiddleware = require('../middleware/custom')


const transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'smaa.gk@gmail.com',
    pass: 'nodemonjs',
  },
});


function sendEmailToken(email,data){


  jwt.sign(
    {
      aspirante: _.pick(data, 'id'),
    },
    'lukeyosoytupadre',
    {
      expiresIn: '2d',
    },
    (err, emailToken) => {
      const url = `http://165.22.178.175:3000/api/confirmation/${emailToken}`;
      //const url = `http://localhost:3000/api/confirmation/${emailToken}`;

      transporter.sendMail({
        to: email,
        subject: 'Confirma tu email SISBE',
        html: `Porfavor da click en este link para confirmar tu correo electrónico: <a href="${url}">${url}</a>`,
      },(err,info)=>{
        if(err){
          console.log(err);
        }
      });
    },
  );
}



function recoverPassword(email,data){


  jwt.sign(
    {
      aspirante: _.pick(data, 'id'),
    },
    'lukeyosoytupadre',
    {
      expiresIn: '1d',
    },
    (err, emailToken) => {
      const url = `http://165.22.178.175:3000/api/recoverpassword/${emailToken}`;
      //const url = `http://localhost:3000/api/confirmation/${emailToken}`;

      transporter.sendMail({
        to: email,
        subject: 'Restablece tu contraseña',
        html: `Porfavor da click en este link para <a href="${url}">Restablecer Contraseña</a>`,
      },(err,info)=>{
        if(err){
          console.log(err);
        }
      });
    },
  );
}

module.exports = {
  async auth(req, res){
    const { email , password } = req.body;
    console.log(req.body);
    const [error, aspirante] = await to(Aspirante.findOne({ where: { email: email } }));

    console.log(aspirante);
    if (error) return responseError(res, 'Error al encontrar al aspirante');

    if (!aspirante){
      return responseError(res, "El usuario con el que intentas ingresar no existe. ");
    }else if(aspirante.estado_cuenta == "inactiva"){
      responseError(res, 'La cuenta a la que estas tratando de acceder esta inactiva.')
    }else if(aspirante.password != req.body.password) {
      responseError(res, 'La contraseña proporcionada es incorrecta')
    }else {

      // if user is found and password is right
      // create a token
      jwt.sign(
        {
          aspirante: _.pick(aspirante, 'email'),
        },
        'lukeyosoytupadre',
        {
          expiresIn: '2d',
        },
        (err, token) => {
          return responseSuccess(res, { token: token}, 201);
        },
      );

      
     
    }

   
  },
  async create(req, res) {
    const { email} = req.body;
    const estado_cuenta = "inactiva"
    const [error, aspirante] = await to(Aspirante.findOrCreate({where: {email: email}, defaults: {estado_cuenta,email}}));
    if (error) return responseError(res, error, 422);

    if(aspirante[1] == true){
      console.log("El aspirante se esta creando, enviando token de verificación")
      sendEmailToken(email,aspirante[0].dataValues);
    }
    return responseSuccess(res, { aspirante: aspirante[0].dataValues, created : aspirante[1] }, 201);
  },
  async getAll(req, res) {
    const [error, aspirantes] = await to(Aspirante.findAll());

    if (error) return responseError(res, error, 422);

    return responseSuccess(res, { aspirantes });
  },
  async resendTokenVerification(req, res) {
    const { aspirante } = req;
    if(aspirante.estado_cuenta == "inactiva"){
      console.log("cuenta inactiva")
      sendEmailToken(aspirante.email,aspirante);
    }
    return responseSuccess(res, { aspirante });
  },
  async recoverPassword(req, res) {
    const { aspirante } = req;
    if(aspirante.estado_cuenta == "activa"){
      console.log("cuenta activa")
      recoverPassword(aspirante.email,aspirante);
      return responseSuccess(res, {"message" : "Revisa tu correo electónico para restablecer tu contraseña"});
    }else if(aspirante.estado_cuenta == "inactiva"){
      return responseSuccess(res, {"message" : "Cuenta inactiva"});
    }else{
      return responseSuccess(res, {"message" : "Cuenta inexistente"});
    }
   
  },
  async updatePassword(req, res){
    console.log(req.decoded)

    try{
      const {aspirante : {id}} = req.decoded;
      await Aspirante.update({password : req.body.password},{where : {id}})
  }catch(e){
      return res.send('error')
  }

  return responseSuccess(res, {"message" : "passwordCambiado"});

  },
 /* async updateData(req, res){
    console.log(req.decoded)

      try{
        const {aspirante : {id}} = req.decoded;
        await Aspirante.update({nombre : req.body.nombre,primer_apellido : req.body.primer_apellido,
          segundo_apellido : req.body.segundo_apellido,boleta :req.body.boleta,no_creditos : req.body.no_creditos},{where : {id}})
    }catch(e){
        return res.send('error')
    }

    return responseSuccess(res, {"message" : "passwordCambiado"});
  }*/
};