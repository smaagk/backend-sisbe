const { archivos } = require("../models");
const path = require("path");
const fs = require("fs");

const {
  to,
  responseError,
  responseSuccess
} = require("../services/util.services");

module.exports = {
  async create(req, res) {
    const data = req.body;
    const email = req.decoded.aspirante.email;
    const [error, archivosData] = await to(
      archivos.findOrCreate({
        where: { email_aspirante: email, type: data.type },
        defaults: {
          filename: data.filename,
          path: "/files/" + data.filename,
          type: data.type
        }
      })
    );

    if (error) return responseError(res, error, 422);

    if (archivosData[1] == true) {
      return responseSuccess(
        res,
        { datos: archivosData[0].dataValues, created: archivosData[1] },
        201
      );
    } else {
      return responseSuccess(res, { message: "El archivo ya existe" }, 201);
    }
  },
  async getFiles(req, res) {
    const email = req.decoded.aspirante.email;
    const [error, archivosData] = await to(
      archivos.findAll({ where: { email_aspirante: email } })
    );

    if (error) return responseError(res, error, 422);

    return responseSuccess(res, { archivosData });
  },
  async deleteFile(req, res) {
    const data = req.body
    const email = req.decoded.aspirante.email;

    let archivo = await archivos.findOne({ where: { email_aspirante: email, filename: data.filename} }).catch(e => {
      console.log(e.message);
    });

    if (!archivo) {
      console.log("err");
    }
    archivo.destroy();
    res.json({message : 'archivo eliminado'})
  }
};
