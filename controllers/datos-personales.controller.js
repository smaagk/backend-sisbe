const { DatosPersonales } = require("../models");
const {
  to,
  responseError,
  responseSuccess
} = require("../services/util.services");
const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const customMiddleware = require("../middleware/custom");

module.exports = {
  async createOrUpdate(req, res) {
    const {
      nombre,
      primer_apellido,
      segundo_apellido,
      curp,
      email,
      numero_celular,
      numero_fijo,
      domicilio,
      alcaldia_municipio,
      entidad_federativa,
      cp
    } = req.body;

    console.log(req.body);
    const [error, datosPersonales] = await to(
      DatosPersonales.findOrCreate({
        where: { email: req.body.email },
        defaults: {
          nombre: nombre,
          primer_apellido: primer_apellido,
          segundo_apellido: segundo_apellido,
          curp: curp,
          numero_celular: numero_celular,
          numero_fijo: numero_fijo,
          domicilio: domicilio,
          alcaldia_municipio : alcaldia_municipio,
          entidad_federativa: entidad_federativa,
          cp: cp
        }
      })
    );
    if (error) return responseError(res, error, 422);

    if (datosPersonales[1] == true) {
      return responseSuccess(
        res,
        { datos: datosPersonales[0].dataValues, created: datosPersonales[1] },
        201
      );
    } else if (datosPersonales[1] == false) {
      try {
        await DatosPersonales.update(
          {
            nombre: nombre,
            primer_apellido: primer_apellido,
            segundo_apellido: segundo_apellido,
            curp: curp,
            numero_celular: numero_celular,
            numero_fijo: numero_fijo,
            domicilio: domicilio,
            alcaldia_municipio : alcaldia_municipio,
            entidad_federativa: entidad_federativa,
            cp: cp
          },
          { where: { email: req.body.email } }
        );
      } catch (e) {
        return res.send("error");
      }

      return responseSuccess(res, { message: "datos actualizados" });
    }
  },
  async getDatospersonales(req, res) {
    const email = req.decoded.aspirante.email;
    const [error, datospersonales] = await to(
      DatosPersonales.findAll({ where: { email } })
    );

    if (error) return responseError(res, error, 422);

    return responseSuccess(res, { datospersonales });
  }
};
