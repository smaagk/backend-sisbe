const { Egresos, Ingresos } = require('../models');
const { to, responseError, responseSuccess } = require('../services/util.services');

module.exports = {

    async create_update(req, res) {
     
      const {
        renta_hipoteca, luz, agua, predial,telefono,gas,alimentacion,transportacion,celulares,otros
      } = req.body.egresos;

      const email_aspirante = req.decoded.aspirante.email
      console.log(email_aspirante)

      const [errorD, egresoDestroy] = await to(Egresos.destroy({where : {email_aspirante : email_aspirante}}));
  
      const [error, egreso] = await to(Egresos.create({
        renta : renta_hipoteca, luz : luz, agua : agua, predial : predial, telefono : telefono,gas :gas , alimentacion : alimentacion, transportacion : transportacion, celulares : celulares, otros: otros, email_aspirante : email_aspirante }));
  
      ingresos = req.body.ingresos;
      ingresos.forEach(function(e) { e.email_aspirante = email_aspirante });
      console.log(ingresos)
      const [errori, ingresoDestroy] = await to(Ingresos.destroy({where : {email_aspirante : email_aspirante}}));
  
      const [errorbi, ingreso] = await to(Ingresos.bulkCreate(ingresos));
  
      
      if (error) return responseError(res, error, 422);
  
      return responseSuccess(res, { message: "datos guardados"}, 201);
    },

    async getIngresos(req, res) {
      const email_aspirante = req.decoded.aspirante.email
      const [error, ingresos] = await to(Ingresos.findAll({where : {email_aspirante : email_aspirante}}));
  
      if (error) return responseError(res, error, 422);
  
      return responseSuccess(res, { ingresos });
    },

    async getEgresos(req, res) {
      const email_aspirante = req.decoded.aspirante.email
      const [error, egresos] = await to(Egresos.findAll({where : {email_aspirante : email_aspirante}}));
  
      if (error) return responseError(res, error, 422);
  
      return responseSuccess(res, { egresos });
    }
  };