const { DatosContactos } = require('../models');
const { to, responseError, responseSuccess } = require('../services/util.services');
const jwt = require('jsonwebtoken')
const _ = require('lodash');
const customMiddleware = require('../middleware/custom')

module.exports = {
    async createOrUpdate(req, res) {
        const data = req.body;
        const email = req.decoded.aspirante.email
        console.log(data);
        const [error, datosContactos] = await to(DatosContactos.findOrCreate({where: {email_aspirante: email}, 
            defaults: {nombre : data.nombre,
                        primer_apellido : data.primer_apellido,
                        segundo_apellido :data.segundo_apellido,
                        parentesco :data.parentesco,
                        email_contacto :data.email,
                        telefono :data.numero_celular
                         }}));
        
        
        if (error) {
            console.log(error)
            return responseError(res, error, 422)};

        if(datosContactos[1] == true){
            return responseSuccess(res, { datos: datosContactos[0].dataValues, created : datosContactos[1] }, 201);
        }else if(datosContactos[1] == false){
            try{
                await DatosContactos.update({nombre : data.nombre,
                    primer_apellido : data.primer_apellido,
                    segundo_apellido :data.segundo_apellido,
                    parentesco :data.parentesco,
                    email_conntacto :data.email,
                    telefono :data.numero_celular
                     },{where: {email_aspirante: email}})
            }catch(e){
                return res.send('error')
            }
          
            return responseSuccess(res, {"message" : "datos actualizados"});

        }
                     
        

    },
    async getDatosContactos(req, res){
        const email_aspirante = req.decoded.aspirante.email
        const [error, datosContactos] = await to(DatosContactos.findAll({where : {email_aspirante}}));
    
        if (error) return responseError(res, error, 422);
    
        return responseSuccess(res, { datosContactos });
    }
}