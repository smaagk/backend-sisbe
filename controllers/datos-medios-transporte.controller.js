const { DatosMediosDeTransporte } = require('../models');
const { to, responseError, responseSuccess } = require('../services/util.services');


module.exports = {
    async createOrUpdate(req, res) {
        const data = req.body;
        const email = req.decoded.aspirante.email
        console.log(data);
        const [error, datosMediosDeTransporte] = await to(DatosMediosDeTransporte.findOrCreate({
            where: { email_aspirante: email },
            defaults: {
                taxi: data.taxi,
                automovil_propio: data.automovil_propio,
                metro: data.metro,
                metrobus: data.metrobus,
                combi: data.combi,
                suburbano: data.suburbano,
                otro: data.otro
            }
        }));

        if (error) return responseError(res, error, 422);

        if (datosMediosDeTransporte[1] == true) {
            return responseSuccess(res, { datos: datosMediosDeTransporte[0].dataValues, created: datosMediosDeTransporte[1] }, 201);
        } else if (datosMediosDeTransporte[1] == false) {
            try {
                await DatosMediosDeTransporte.update({
                    taxi: data.taxi,
                    automovil_propio: data.automovil_propio,
                    metro: data.metro,
                    metrobus: data.metrobus,
                    combi: data.combi,
                    suburbano: data.suburbano,
                    otro: data.otro
                }, { where: { email_aspirante: email } })
            } catch (e) {
                return res.send('error')
            }

            return responseSuccess(res, { "message": "datos actualizados" });

        }



    },
    async getDatosMediosDeTransporte(req, res) {
        const email = req.decoded.aspirante.email
        const [error, datosMediosDeTransporte] = await to(DatosMediosDeTransporte.findAll({ where: { email_aspirante: email } }));

        if (error) return responseError(res, error, 422);

        return responseSuccess(res, { datosMediosDeTransporte });
    }
}