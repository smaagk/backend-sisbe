const { statusFormularios } = require("../models");
const {
  to,
  responseError,
  responseSuccess
} = require("../services/util.services");


module.exports = {
    async createOrUpdate(req, res) {
        const data = req.body;
        const email = req.decoded.aspirante.email
        console.log(data);
        const [error, _statusFormularios] = await to(statusFormularios.findOrCreate({
            where: { email_aspirante: email },
            defaults: {
                datos_personales: data.datos_personales,
                estudio_socioeconomico: data.estudio_socioeconomico,
                ingresos_egresos: data.ingresos_egresos,
                archivos: data.archivos
            }
        }));

        if (error) return responseError(res, error, 422);

        if (_statusFormularios[1] == true) {
            return responseSuccess(res, { datos: _statusFormularios[0].dataValues, created: _statusFormularios[1] }, 201);
        } else if (_statusFormularios[1] == false) {
            try {
                await statusFormularios.update({
                    datos_personales: data.datos_personales,
                    estudio_socioeconomico: data.estudio_socioeconomico,
                    ingresos_egresos: data.ingresos_egresos,
                    archivos: data.archivos
                }, { where: { email_aspirante: email } })
            } catch (e) {
                return res.send('error')
            }

            return responseSuccess(res, { "message": "datos actualizados" });

        }



    },
    async getStatusFormularios(req, res) {
        const email = req.decoded.aspirante.email
        const [error, _statusFormularios] = await to(statusFormularios.findAll({ where: { email_aspirante: email } }));

        if (error) return responseError(res, error, 422);

        return responseSuccess(res, { _statusFormularios });
    }
}
