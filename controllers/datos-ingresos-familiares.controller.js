const { DatosIngresoFamiliares } = require('../models');
const { to, responseError, responseSuccess } = require('../services/util.services');


module.exports = {
    async createOrUpdate(req, res) {
        const data = req.body;
        const email = req.decoded.aspirante.email
        console.log(data);
        const [error, datosIngresoFamiliar] = await to(DatosIngresoFamiliares.findOrCreate({where: {email_aspirante: email}, 
            defaults: {no_integrantes : data.no_integrantes,
                        trabaja : data.trabaja,
                        viviendo_con :data.viviendo_con,
                        dependencia :data.dependencia,
                        sosten :data.sosten,
                        no_personas_sosten_padres :data.no_personas_sosten_padres,
                        no_personas_aporte : data.no_personas_aporte,
                        servicio_medico : data.servicio_medico,
                        clase_social : data.clase_social
                         }}));
        
        if (error) return responseError(res, error, 422);

        if(datosIngresoFamiliar[1] == true){
            return responseSuccess(res, { datos: datosIngresoFamiliar[0].dataValues, created : datosIngresoFamiliar[1] }, 201);
        }else if(datosIngresoFamiliar[1] == false){
            try{
                await DatosVivienda.update({no_integrantes : data.no_integrantes,
                    trabaja : data.trabaja,
                    viviendo_con :data.viviendo_con,
                    dependencia :data.dependencia,
                    sosten :data.sosten,
                    no_personas_sosten_padres :data.no_personas_sosten_padres,
                    no_personas_aporte : data.no_personas_aporte,
                    servicio_medico : data.servicio_medico,
                    clase_social : data.clase_social
                    },{where: {email_aspirante: email}})
            }catch(e){
                return res.send('error')
            }
          
            return responseSuccess(res, {"message" : "datos actualizados"});

        }
                     
        

    },
    async getDatosIngresoFamiliar(req, res){
        const email = req.decoded.aspirante.email
        const [error, datosIngresoFamiliar] = await to(DatosIngresoFamiliares.findAll({where : {email_aspirante : email}}));
    
        if (error) return responseError(res, error, 422);
    
        return responseSuccess(res, { datosIngresoFamiliar });
    }
}