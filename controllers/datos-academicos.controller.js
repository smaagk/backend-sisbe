const { DatosAcademicos } = require('../models');
const { to, responseError, responseSuccess } = require('../services/util.services');
const jwt = require('jsonwebtoken')
const _ = require('lodash');
const customMiddleware = require('../middleware/custom')

module.exports = {
    async createOrUpdate(req, res) {
        const data = req.body;
        const email = req.decoded.aspirante.email
        console.log(data);
        const [error, datosAcademicos] = await to(DatosAcademicos.findOrCreate({where: {email: email}, 
            defaults: {boleta : data.boleta,
                        no_creditos : data.no_creditos_inscritos,
                        promedio :data.promedio,
                        situacion :data.situacion_escolar,
                        unidad :data.unidad_academica,
                        modalidad :data.modalidad
                         }}));
        
        if (error) return responseError(res, error, 422);

        if(datosAcademicos[1] == true){
            return responseSuccess(res, { datos: datosAcademicos[0].dataValues, created : datosAcademicos[1] }, 201);
        }else if(datosAcademicos[1] == false){
            try{
                await DatosAcademicos.update({boleta : data.boleta,
                    no_creditos : data.no_creditos_inscritos,
                    promedio :data.promedio,
                    situacion :data.situacion_escolar,
                    unidad :data.unidad_academica,
                    modalidad :data.modalidad
                    },{where: {email: email}})
            }catch(e){
                return res.send('error')
            }
          
            return responseSuccess(res, {"message" : "datos actualizados"});

        }
                     
        

    },
    async getDatosacademicos(req, res){
        const email = req.decoded.aspirante.email
        const [error, datosacademicos] = await to(DatosAcademicos.findAll({where : {email}}));
    
        if (error) return responseError(res, error, 422);
    
        return responseSuccess(res, { datosacademicos });
    }
}