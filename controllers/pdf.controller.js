const pdf = require("html-pdf");
module.exports = {
  generate(req, res) {
    
    let config = req.body.config;
    let contenido = new Buffer(req.body.contenido, "base64").toString(
      "utf8"
    );

    pdf.create(contenido, config).toBuffer(function(err, buffer) {
      //console.log('This is a buffer:', Buffer.isBuffer(buffer));
      res.json(Buffer.from(buffer).toString("base64"));
    });
  }
};