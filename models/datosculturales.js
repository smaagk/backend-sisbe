'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosCulturales = sequelize.define('DatosCulturales', {
    discapacidad: DataTypes.STRING,
    discapacidad_familiar: DataTypes.BOOLEAN,
    deporte: DataTypes.STRING,
    frecuencia_deporte: DataTypes.STRING,
    actividad_artistica: DataTypes.STRING,
    tiempo_libre: DataTypes.STRING,
    p_contaminacion: DataTypes.STRING,
    p_escasez_agua: DataTypes.STRING,
    p_energia: DataTypes.STRING,
    p_areasverdes: DataTypes.STRING,
    p_contaminacion_agua: DataTypes.STRING,
    reciclaje: DataTypes.STRING,
    programa_proteccion_ambiental: DataTypes.BOOLEAN,
    programa_servicio: DataTypes.BOOLEAN,
    tipo_programa: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  DatosCulturales.associate = function(models) {
    // associations can be defined here
  };
  return DatosCulturales;
};