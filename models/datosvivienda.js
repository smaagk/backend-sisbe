'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosVivienda = sequelize.define('DatosVivienda', {
    residencia_actual: DataTypes.STRING,
    propiedad_estatus: DataTypes.STRING,
    zona: DataTypes.STRING,
    computadora: DataTypes.BOOLEAN,
    telefono: DataTypes.BOOLEAN,
    lavadora: DataTypes.BOOLEAN,
    licuadora: DataTypes.BOOLEAN,
    television: DataTypes.BOOLEAN,
    refrigerador: DataTypes.BOOLEAN,
    celular: DataTypes.BOOLEAN,
    internet: DataTypes.BOOLEAN,
    agua_potable: DataTypes.BOOLEAN,
    luz: DataTypes.BOOLEAN,
    gas: DataTypes.BOOLEAN,
    drenaje: DataTypes.BOOLEAN,
    cuarto_cocina: DataTypes.BOOLEAN,
    no_banos: DataTypes.STRING,
    no_recamaras: DataTypes.STRING,
    personas_viviendo: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  DatosVivienda.associate = function(models) {
    // associations can be defined here
  };
  return DatosVivienda;
};