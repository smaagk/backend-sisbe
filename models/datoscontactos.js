'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosContactos = sequelize.define('DatosContactos', {
    nombre: DataTypes.STRING,
    primer_apellido: DataTypes.STRING,
    segundo_apellido: DataTypes.STRING,
    parentesco: DataTypes.STRING,
    email_contacto: DataTypes.STRING,
    telefono: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  DatosContactos.associate = function(models) {
    // associations can be defined here
  };
  return DatosContactos;
};