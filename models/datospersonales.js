'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosPersonales = sequelize.define('DatosPersonales', {
    nombre: DataTypes.STRING,
    primer_apellido: DataTypes.STRING,
    segundo_apellido: DataTypes.STRING,
    curp: DataTypes.STRING,
    email: DataTypes.STRING,
    numero_celular: DataTypes.STRING,
    numero_fijo: DataTypes.STRING,
    domicilio: DataTypes.STRING,
    alcaldia_municipio: DataTypes.STRING,
    entidad_federativa: DataTypes.STRING,
    cp: DataTypes.STRING
  }, {});
  DatosPersonales.associate = function(models) {
    // associations can be defined here
  };
  return DatosPersonales;
};