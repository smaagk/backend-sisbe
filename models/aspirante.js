'use strict';
module.exports = (sequelize, DataTypes) => {
  const Aspirante = sequelize.define('Aspirante', {
    email : DataTypes.STRING,
    password: DataTypes.STRING,
    estado_cuenta: DataTypes.STRING,
  }, {});
  Aspirante.associate = function(models) {
    // associations can be defined here
  };
  return Aspirante;
};