'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosIngresoFamiliares = sequelize.define('DatosIngresoFamiliares', {
    no_integrantes: DataTypes.STRING,
    trabaja: DataTypes.BOOLEAN,
    viviendo_con: DataTypes.STRING,
    dependencia: DataTypes.BOOLEAN,
    sosten: DataTypes.STRING,
    no_personas_sosten_padres: DataTypes.STRING,
    no_personas_aporte: DataTypes.STRING,
    servicio_medico: DataTypes.STRING,
    clase_social: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  DatosIngresoFamiliares.associate = function(models) {
    // associations can be defined here
  };
  return DatosIngresoFamiliares;
};