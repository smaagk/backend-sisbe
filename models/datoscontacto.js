'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosContacto = sequelize.define('DatosContacto', {
    nombre: DataTypes.STRING,
    primer_apellido: DataTypes.STRING,
    segundo_apellido: DataTypes.STRING,
    parentesco: DataTypes.STRING,
    email_contacto: DataTypes.STRING,
    telefono: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  DatosContacto.associate = function(models) {
    // associations can be defined here
  };
  return DatosContacto;
};