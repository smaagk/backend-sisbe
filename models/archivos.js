'use strict';
module.exports = (sequelize, DataTypes) => {
  const archivos = sequelize.define('archivos', {
    email_aspirante: DataTypes.STRING,
    filename: DataTypes.STRING,
    path: DataTypes.STRING,
    type: DataTypes.STRING
  }, {});
  archivos.associate = function(models) {
    // associations can be defined here
  };
  return archivos;
};