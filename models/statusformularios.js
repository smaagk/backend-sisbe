'use strict';
module.exports = (sequelize, DataTypes) => {
  const statusFormularios = sequelize.define('statusFormularios', {
    datos_personales: DataTypes.BOOLEAN,
    estudio_socioeconomico: DataTypes.BOOLEAN,
    ingresos_egresos: DataTypes.BOOLEAN,
    archivos: DataTypes.BOOLEAN,
    email_aspirante: DataTypes.STRING
  }, {});
  statusFormularios.associate = function(models) {
    // associations can be defined here
  };
  return statusFormularios;
};