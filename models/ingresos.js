'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ingresos = sequelize.define('Ingresos', {
    nombre: DataTypes.STRING,
    edad: DataTypes.STRING,
    parentesco: DataTypes.STRING,
    estado_civil: DataTypes.STRING,
    ingresos: DataTypes.STRING,
    ocupacion: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  Ingresos.associate = function(models) {
    // associations can be defined here
  };
  return Ingresos;
};