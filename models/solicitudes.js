'use strict';
module.exports = (sequelize, DataTypes) => {
  const solicitudes = sequelize.define('solicitudes', {
    email_aspirante: DataTypes.STRING,
    boleta: DataTypes.STRING,
    nombre: DataTypes.STRING,
    primer_apellido: DataTypes.STRING,
    segundo_apellido: DataTypes.STRING,
    estatus: DataTypes.STRING,
    ciclo_escolar: DataTypes.STRING
  }, {});
  solicitudes.associate = function(models) {
    // associations can be defined here
  };
  return solicitudes;
};