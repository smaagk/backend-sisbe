'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosAcademicos = sequelize.define('DatosAcademicos', {
    boleta: DataTypes.STRING,
    no_creditos: DataTypes.STRING,
    promedio: DataTypes.STRING,
    situacion: DataTypes.STRING,
    unidad: DataTypes.STRING,
    modalidad: DataTypes.STRING,
    email: DataTypes.STRING
  }, {});
  DatosAcademicos.associate = function(models) {
    // associations can be defined here
  };
  return DatosAcademicos;
};