'use strict';
module.exports = (sequelize, DataTypes) => {
  const DatosMediosDeTransporte = sequelize.define('DatosMediosDeTransporte', {
    taxi: DataTypes.STRING,
    automovil_propio: DataTypes.STRING,
    metro: DataTypes.STRING,
    metrobus: DataTypes.STRING,
    combi: DataTypes.STRING,
    suburbano: DataTypes.STRING,
    otro: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  DatosMediosDeTransporte.associate = function(models) {
    // associations can be defined here
  };
  return DatosMediosDeTransporte;
};