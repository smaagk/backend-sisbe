'use strict';
module.exports = (sequelize, DataTypes) => {
  const Egresos = sequelize.define('Egresos', {
    renta: DataTypes.STRING,
    agua: DataTypes.STRING,
    luz: DataTypes.STRING,
    predial: DataTypes.STRING,
    telefono: DataTypes.STRING,
    gas: DataTypes.STRING,
    alimentacion: DataTypes.STRING,
    transportacion: DataTypes.STRING,
    celulares: DataTypes.STRING,
    otros: DataTypes.STRING,
    email_aspirante: DataTypes.STRING
  }, {});
  Egresos.associate = function(models) {
    // associations can be defined here
  };
  return Egresos;
};